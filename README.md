gard3n—the flow3r network game
==============================

Gard3n is a simple game about making connections.

You play it by launching it on two badges, then connecting them via jack (always the left jack to the right one!).

The app displays the number of unique badges you have connected to.
There's also another number—if you manage to find me (@mewp), or someone who connected to me, you get a gard3n number—your connection distance to me (think: euler number, but for badges!).

The leds display the colors based on the first 40 badges you connected to. They are... inaccurate for now, I'll debug that eventually.
