from st3m.application import Application, ApplicationContext
import st3m.run
import badgenet
import badgelink
from select import poll, POLLIN
import socket
from binascii import unhexlify, hexlify
import audio
import leds

class Gard3n(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.number = -1
        badgenet.configure_jack(badgenet.SIDE_LEFT, badgenet.MODE_ENABLE_AUTO)
        badgenet.configure_jack(badgenet.SIDE_RIGHT, badgenet.MODE_ENABLE_AUTO)
        self.addr = "ff02::1%" + badgenet.get_interface().name()
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.sock.bind((self.addr, 2137))
        self.time_since_last_broadcast = 0
        self.poll = poll()
        self.poll.register(self.sock, POLLIN)
        self.leds = []

        try:
            ips = open('/sd/gard3n_data', 'rb').read()
            self.ips = list(ips[i:i+6] for i in range(len(ips)//6))
            for i in range(min(len(self.ips), 40)):
                self.leds.append(int(hexlify(self.ips[i]), 16) % 6 + 1)
        except:
            try:
                ips = open('/flash/gard3n_data', 'rb').read()
                self.ips = list(ips[i:i+6] for i in range(len(ips)//6))
                for i in range(min(len(self.ips), 40)):
                    self.leds.append(int(hexlify(self.ips[i]), 16) % 6 + 1)
            except:
                self.ips = list()

        try:
            self.number = int(open('/sd/gard3n_number', 'r').read())
        except:
            try:
                self.number = int(open('/flash/gard3n_number', 'r').read())
            except:
                self.number = -1

        self.color_idx = int(badgenet.get_interface().ifconfig6()[0].split(':')[-1], 16) % 6 + 1

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb((self.color_idx & 4) // 4, (self.color_idx & 2) // 2, self.color_idx & 1).move_to(0,-10)
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 260 if len(self.ips) < 100 else 180
        ctx.font = ctx.get_font_name(5)
        ctx.text(str(len(self.ips)))
        if self.number >= 0:
            ctx.move_to(0, 70)
            ctx.font_size = 20
            ctx.font = ctx.get_font_name(1)
            ctx.text(f"gard3n number: {self.number}")

        for i in range(min(len(self.leds), 40)):
            leds.set_rgb(i, (self.leds[i] & 4) // 4, (self.leds[i] & 2) // 2, self.leds[i] & 1)
        leds.update()


    def think(self, ins: InputState, delta_ms: int) -> None:
        if not badgelink.left.active() and audio.headphones_are_connected():
            badgelink.left.enable()
        if not badgelink.right.active() and audio.line_in_is_connected():
            badgelink.right.enable()
        self.time_since_last_broadcast += delta_ms
        if self.time_since_last_broadcast > 500:
            self.time_since_last_broadcast -= 500
            self.sock.sendto(str(self.number), (self.addr, 2137))

        ip_candidates = set()
        for s, _ in self.poll.poll(1):
            data, addr = s.recvfrom(1024)
            if int(data) >= 0 and (self.number == -1 or self.number > int(data) + 1):
                self.number = int(data) + 1
                try:
                    f = open('/sd/gard3n_number', 'w')
                    f.write(str(self.number))
                    f.close()
                except:
                    pass
                f = open('/flash/gard3n_number', 'w')
                f.write(str(self.number))
                f.close()

            byteaddr = b''.join([unhexlify('{:04x}'.format(int(x, 16))) for x in addr[0].split(':')[-3:]])
            ip_candidates.add(byteaddr)

        if ip_candidates and ip_candidates - set(self.ips):
            for addr in ip_candidates - set(self.ips):
                if len(self.leds) < 40:
                    self.leds.append(int(hexlify(addr), 16) % 6 + 1)
            self.ips += list(ip_candidates)
            try:
                f = open('/sd/gard3n_data', 'wb')
                f.write(b''.join(self.ips))
                f.close()
            except:
                pass
            f = open('/flash/gard3n_data', 'wb')
            f.write(b''.join(self.ips))
            f.close()

if __name__ == "__main__":
    st3m.run.run_responder(Gard3n(ApplicationContext()))
